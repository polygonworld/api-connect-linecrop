<?php
class ControllerApiTcProduct extends Controller {
    public function getList() {
        $this->load->language('api/order');
        $this->load->model('api/order');
        $json = array();

        if (!$this->model_api_order->getToken($this->request->get['token'])) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            $this->load->model('api/order');
            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }
            if (isset($this->request->get['ids'])) {
                $filter_id = $this->request->get['ids'];
            } else {
                $filter_id = 0;
            }
            $results = $this->model_api_order->getProducts($filter_data = array(
                'product_id' => $filter_id,
                'start' => ($page - 1) * $this->request->get['limit'],
                'limit' => $this->request->get['limit']
            ));

            if ($results) {
                $listProducts = array();
                if (sizeof($results > 0)) {
                    foreach ($results as $result) {
                        $result['images'] = $this->model_api_order->getProductImages($result['product_id']);
                        $listProducts[] = $result;
                    }
                }
                $json['status'] 	= true;
                $json['products'] 	= $listProducts;
            } else {
                $json['status'] 	= false;
                $json['error'] = $this->language->get('error_not_found');
            }
        }


        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function countProduct() {
        $this->load->language('api/order');
        $this->load->model('api/order');
        $json = array();

        $results = $this->model_api_order->getTotalProducts($filter_data = array());

        if ($results > 0) {
            $json['status'] 	= true;
            $json['count'] 	= $results;
        } else {
            $json['status'] 	= false;
            $json['error'] = $this->language->get('error_not_found');
        }


        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function updateQtyProduct() {
        $this->load->language('api/cart');
        $this->load->model('api/order');
        $json = array();
        $postdata = file_get_contents("php://input");
        $dataBody = json_decode($postdata);
        if (!$this->model_api_order->getToken($this->request->get['token'])) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            $product_id = (int)$dataBody->product_id;
            $quantity = (int)$dataBody->inventory_quantity;

            if (!empty($this->model_api_order->getProduct($product_id))) {
                $this->model_api_order->updateQtyProduct($product_id, $quantity);
                $json['status'] = true;
            } else {
                $json['status'] = false;
            }
        }

        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}