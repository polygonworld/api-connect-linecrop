<?php
class ControllerApiTcOrder extends Controller {
    public function getList() {
        $this->load->language('api/order');
        $this->load->model('api/order');
        $json = array();

        if (!$this->model_api_order->getToken($this->request->get['token'])) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            $this->load->model('api/order');
            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }
            if (isset($this->request->get['ids'])) {
                $filter_id = $this->request->get['ids'];
            } else {
                $filter_id = '';
            }

            if (isset($this->request->get['datelastcreated'])) {
                $filter_datecreated = $this->request->get['datelastcreated'];
            } else {
                $filter_datecreated = '';
            }


            $results = $this->model_api_order->getOrders($filter_data = array(
                'sort'                 => 'o.order_id',
                'order'                => 'DESC',
                'filter_order_id'      => $filter_id,
                'filter_date_added'    => $filter_datecreated,
                'start' => ($page - 1) * $this->request->get['limit'],
                'limit' => $this->request->get['limit']
            ));

            if ($results) {
                $listOrders = array();
                if (sizeof($results > 0)) {
                    foreach ($results as $result) {
                        $listOrders[] = $this->model_api_order->getOrder($result['order_id']);
                    }
                }
                $json['status'] 	= true;
                $json['orders'] 	= $listOrders;
            } else {
                $json['status'] 	= false;
                $json['error'] = $this->language->get('error_not_found');
            }
        }


        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }


    public function countOrder() {
        $this->load->language('api/order');
        $this->load->model('api/order');
        $json = array();

        $results = $this->model_api_order->getTotalOrders($filter_data = array());

        if ($results > 0) {
            $json['status'] 	= true;
            $json['count'] 	= $results;
        } else {
            $json['status'] 	= false;
            $json['error'] = $this->language->get('error_not_found');
        }


        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}