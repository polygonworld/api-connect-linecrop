<?php
class ControllerApiTcLogin extends Controller {
    public function index() {
        $this->load->language('api/login');

        $json = array();

        $this->load->model('api/login');

        // Login with API Key
        $api_info = $this->model_api_login->getApiByKey($this->request->post['key']);
        if ($api_info) {
            $this->model_api_login->addIpToKey($api_info['api_id'], $this->request->server['REMOTE_ADDR']);
            // Check if IP is allowed
            $ip_data = array();

            $results = $this->model_api_login->getApiIps($api_info['api_id']);

            foreach ($results as $result) {
                $ip_data[] = trim($result['ip']);
            }

            if (!in_array($this->request->server['REMOTE_ADDR'], $ip_data)) {
                $json['error']['ip'] = sprintf($this->language->get('error_ip'), $this->request->server['REMOTE_ADDR']);
            }

            if (!$json) {
                $json['success'] = $this->language->get('text_success');

                // We want to create a seperate session so changes do not interfere with the admin user.

                $session_id_new = '78a8597d4ed5f64019b9ca3c86';

                $this->session->start('api', $session_id_new);

                $this->session->data['api_id'] = $api_info['api_id'];

                // Close and write the new session.
                //$session->close();

                $this->session->start('default');

                // Create Token
                $json['api_id'] = $api_info['api_id'];
                $json['token'] = $this->model_api_login->addApiSession($api_info['api_id'], $session_id_new, $this->request->server['REMOTE_ADDR']);
            } else {
                $json['error']['key'] = $this->language->get('error_key');
            }
        }

        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
